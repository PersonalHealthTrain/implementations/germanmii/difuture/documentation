# PHT Station

This section will provide installation instructions for installing a PHT Station. It assumes that the station has been
registered in the UI.

## Installation
Visit
the [station repository](https://github.com/PHT-Medic/station)
to view the code (the installation instructions can also be found here).

### Requirements

[Docker](https://docs.docker.com/get-docker/) and [docker-compose](https://docs.docker.com/compose/install/) need to be
installed. For the default installation to work the ports `8080` and `5432` need to be available on localhost. 


### Clone the repository

```shell
git clone https://github.com/PHT-Medic/station.git
```


### Configure the station environment
The station requires several environments variables to be set to be properly configured.
It requires credentials to access the [central container registry](https://harbor-pht.tada5hi.net/). The station
identifier needs to be set and a private key provided as a volume. Most of these (with the exception of the private key) 
values are provided (and can be configured) in the [central user interface](user_interface.md).  
Additionally, a default FHIR server and credentials for its can be configured via some environment. This server will be 
used when a train is not configured with an individual FHIR warehouse.  

The best to set these environment variables in the `.env` file at the root of this repository. Optionally you can also
set them directly in the `docker-compose.yml` file.

Open the `.env` file and configure the variables according to the following sections.

#### Container registry access

These configuration values can be accessed in the central UI under your realm's settings.

  - `HARBOR_API_URL` the url of the central harbor instance
  - `HARBOR_USER` username to authenticate against harbor
  - `HARBOR_PW` password to authenticate against harbor
!!! info
    Make sure to replace the `$` in your robot user account with `$$` if you set the variable directly in the `docker-compose.yml` file

#### Provide station identification
The station identifier can also be found in the realm settings of the central UI.  
It also provides the option to upload
a public key, which needs to be derived from the key specified in the `STATION_PRIVATE_KEY_PATH` environment variable.

- `STATION_PRIVATE_KEY_PATH` absolute path to a `PEM` formatted RSA private key.
- `STATION_ID` pseudo identifier of the station which (modify and access in central UI)

#### Configure airflow admin user
This user:password combination will be used as an admin account for the [airflow webserver](https://airflow.apache.org/docs/apache-airflow/stable/security/webserver.html)
that is operated as the central component of the PHT station.  
This user can be used to create additional user accounts with different permissions.

- `AIRFLOW_USER` username for the admin user to be created for the airflow instance 
- `AIRFLOW_PW` password for the airflow admin user

#### Configure default fhir server
The following environment variables configure the default FHIR server and its login credentials.  
If the `FHIR_ADDRESS` environment variable is set the environment will be scanned for login credentials.
!!! warning
    Conflicting authentication information will lead to errors, make user only one of `FHIR_USER:FHIR_PW`, `FHIR_TOKEN`,
    or `OIDC` configuration is used


 - `FHIR_ADDRESS` the address of the default fhir server connected to the station (this can also be configured per train)
 - `FHIR_USER` username to authenticate against the FHIR server using Basic Auth
 - `FHIR_PW` password for Basic Auth
 - `FHIR_TOKEN` static token to authenticate against the FHIR server using Bearer Token
 - `CLIENT_ID` identifier of client with permission to access the fhir server 
 - `CLIENT_SECRET` secret of above client to authenticate against the provider
 - `OIDC_PROVIDER_URL` token url of Open ID connect provider e.g. keycloak, that is configured for the FHIR server
 - `FHIR_SERVER_TYPE` the type of fhir server (PHT FHIR client supports IBM, Hapi and Blaze FHIR servers)
 

### Start the station using docker-compose

1. Create a docker volume for the station database with 
   ```
   docker volume create --name=pg_station
   ```
2. Bring up the project by running
   ```
   docker-compose up -d
   ```
3. Check the logs for any errors while bringing up the project
   ```
   docker-compose logs
   ```


## Getting started with Airflow
Trains and other station tasks are executed via airflow DAGs. The DAGs can be triggered via the airflow web interface,
which is available under `http://127.0.0.1:8080` on the station machine. 
The execution of the DAGs can also be monitored in the webinterface

### Login
The first time you access the webinterface you will be prompted to log in. Enter the credentials set in the `.env` file 
to login as admin.

![Airflow Login](images/station_images/airflow_login.png)

### Triggering the test DAG
To test the configuration of the station as defined in the `.env` file, trigger the DAG named `test_station_configuration`
in the user interface.  
A DAG is triggered in the UI by clicking on the "play" button, where it can be started either with or without a json 
file containing additional configuration for the DAG run.

![Airflow UI Dags](images/station_images/airflow_ui.png)

Trigger the DAG without any additional configuration to check if the station is properly configured. A notification should
appear in the UI that the DAG has been triggered.  

To monitor the execution click on the name of the DAG. You should see the individual tasks contained in the DAG as well as
their status in the UI. If all tasks are marked as success, the station is properly configured and can connect to harbor
as well as a FHIR server.

![Airflow UI test station config](images/station_images/test_config_dag.png)

### Accessing logs

The logs stored during the execution of a DAG can be accessed for each individual task by clicking the indicator next 
to the name of the task and selection Log in the pop-up window that appears.

![Airflow UI access logs](images/station_images/task_logs.png)

If there are any errors stacktraces can be found in these logs, as well as any other output of the tasks (stdout, stderr)

![Airflow UI task log details](images/station_images/task_log_details.png)

### Running a train

To execute a train that is available for your station, trigger the `run_train` DAG, with configuration options specifying
the train image to be pulled from harbor and executed as well as additional environment variables or volumes. A template
train configuration is displayed below. 

```json
{
  "repository": "<HARBOR-REGISTRY>/<STATION_ID>/<TRAIN-IMAGE>",
  "tag": "latest",
  "env": {"FHIR_ADDRESS": "<FHIR-ADRESS>","FHIR_USER": "<ID>","FHIR_PW": "<PSW>"}
}
```

Replace the placeholders with the values of the train image to execute, and other
variables with the values corresponding to the stations configuration and paste it into the configuration form shown in 
the following image.

![Airflow trigger run train](images/station_images/trigger_run_train.png)
