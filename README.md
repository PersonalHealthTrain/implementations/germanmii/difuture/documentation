# PHT Documentation

This repository contains markdown files and images to be compiled into the PHT Documentation and User guide
on [readthedocs.org](https://readthedocs.org/)

## Contributing

1. Install the requirements: `pip install -r requirements.txt`
2. Use mkdocs to display the site with hot reloading run: `mkdocs serve`
3. Any changes made in the `./docs` directory or `mkdocs.yml` will be hot reloaded on save